# devops

**Instruction to run application**

**Prerequisites**
- install git
- clone repositories to one directory
- install npm
- install angular cli
- install maven
- install jdk
- install docker.io
- install docker-compose

**Building images**

Building docker images is simple.

Step 1

Move into devops directory

Step 2

run `./rebuild` script with parameter **all**

**Running application**

In devops directory input command:

`docker-compose up -d`

**First sight on application**

So as to start working on application, at first import data.

There is an icon in top right corner of criteria.

It's only first start action.

Then you can feel free to search for testers by criteria.
