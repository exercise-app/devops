DEVOPS_PWD=$PWD

api () {
  docker rm -f exercise-api
  docker image rm -f exercise:0.0.1-SNAPSHOT
  cd $DEVOPS_PWD/../exercise
  mvn package
}

web () {
  docker rm -f exercise-ui
  docker image rm -f applause/exercise-ui
  cd $DEVOPS_PWD/../exercise-ui
  ng build
  docker build --tag applause/exercise-ui -f Dockerfile .
}

all () {
  api
  web
}

if [[ $# -eq 0 ]] ; then
echo "usage: option[]
      options:
      api : rebuilds exercise-api image
      web : rebuilds exercise-ui image
      all: rebuilds all images
      "
fi

for var in "$@"
do
	if [ "$var" == "api" ]
    then
      echo "rebuilding api"
      api
    elif [ "$var" == "web" ]
    then
      echo "rebuilding web"
      web
    elif [ "$var" == "all" ]
    then
      echo "rebuilding all"
      all
    fi
done
